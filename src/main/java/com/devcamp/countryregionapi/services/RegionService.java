package com.devcamp.countryregionapi.services;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.models.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN", "Ha Noi");
    Region tpHoChiMinh = new Region("HCM", "Tp Ho Chi Minh");
    Region daNang = new Region("DN", "Da Nang");

    Region newyork = new Region("NY", "New York");
    Region florida = new Region("FLO", "Florida");
    Region texas = new Region("TX", "Texas");

    Region hokkaido = new Region("HO", "Hokkaido");
    Region kansai = new Region("KS", "Kansai");
    Region kanto = new Region("KT", "Kanto");

    Region moscow = new Region("MW", "Moscow");
    Region saintPetersburg = new Region("SP", "Saint Petersburg");
    Region krasnodar = new Region("KN", "Krasnodar");

    Region vientiane = new Region("VT", "Vientiane");
    Region luangPrabang = new Region("LP", "Luang Prabang");
    Region champasak = new Region("CP", "Champasak");

    Region seoul = new Region("SE", "Seoul");
    Region busan = new Region("BS", "Busan");
    Region jeju = new Region("JU", "Jeju");
    public ArrayList<Region> getRegionVietNam(){
        ArrayList<Region> regionVietName = new ArrayList<Region>();

        regionVietName.add(hanoi);
        regionVietName.add(tpHoChiMinh);
        regionVietName.add(daNang);

        return regionVietName;
    }

    public ArrayList<Region> getRegionUS(){
        ArrayList<Region> regionUS = new ArrayList<Region>();

        regionUS.add(newyork);
        regionUS.add(florida);
        regionUS.add(texas);

        return regionUS;
    }

    public ArrayList<Region> getRegionJapan(){
        ArrayList<Region> regionJapan = new ArrayList<Region>();

        regionJapan.add(hokkaido);
        regionJapan.add(kansai);
        regionJapan.add(kanto);

        return regionJapan;
    }

    public ArrayList<Region> getRegionKorean(){
        ArrayList<Region> regionKorean = new ArrayList<Region>();


        regionKorean.add(seoul);
        regionKorean.add(jeju);
        regionKorean.add(busan);


        return regionKorean;
    }

    public ArrayList<Region> getRegionRussia(){
        ArrayList<Region> regionRussia = new ArrayList<Region>();

        regionRussia.add(moscow);
        regionRussia.add(saintPetersburg);
        regionRussia.add(krasnodar);

        return regionRussia;
    }

    public ArrayList<Region> getRegionLao(){
        ArrayList<Region> regionLao = new ArrayList<Region>();

        regionLao.add(vientiane);
        regionLao.add(luangPrabang);
        regionLao.add(champasak);

        return regionLao;
    }
    //C2 don het viec cho service
    public Region filterRegion(String regionCode){
        ArrayList<Region> region = new ArrayList<Region>();

        region.add(hanoi);
        region.add(tpHoChiMinh);
        region.add(daNang);
        region.add(newyork);
        region.add(florida);
        region.add(texas);
        region.add(hokkaido);
        region.add(kansai);
        region.add(kanto);
        region.add(seoul);
        region.add(jeju);
        region.add(busan);
        region.add(vientiane);
        region.add(luangPrabang);
        region.add(champasak);
        region.add(moscow);
        region.add(saintPetersburg);
        region.add(krasnodar);

        Region findRegion = new Region();

        for (Region regionElement : region) {
            if(regionElement.getRegionCode().equals(regionCode)){
                findRegion = regionElement;
            }
        }

        return findRegion;
    }

}
