package com.devcamp.countryregionapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.devcamp.countryregionapi.models.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionservice;
    
    Country vietNam = new Country("VN", "Viet Nam");
    Country us = new Country("US", "My");
    Country japan = new Country("JP", "Nhat Ban");
    Country korean = new Country("KR", "Han Quoc");
    Country russia = new Country("RU", "Nga");
    Country lao = new Country("LA", "Lao");

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = new ArrayList<Country>();
        
        vietNam.setRegions(regionservice.getRegionVietNam());
        us.setRegions(regionservice.getRegionUS());
        japan.setRegions(regionservice.getRegionJapan());
        korean.setRegions(regionservice.getRegionKorean());
        russia.setRegions(regionservice.getRegionRussia());
        lao.setRegions(regionservice.getRegionLao());

        allCountry.add(vietNam);
        allCountry.add(us);
        allCountry.add(japan);
        allCountry.add(korean);
        allCountry.add(russia);
        allCountry.add(lao);


        return allCountry;
    }
    public Country getCountryByIndex(@PathVariable("id") int index) {
        ArrayList<Country> allCountry = getAllCountries();
        if (index >= 0 && index < allCountry.size()) {
            return allCountry.get(index);
        } else {
            return null;
        }
    }
}
